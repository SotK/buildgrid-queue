# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os
import sys
from urllib.parse import urlparse

import click
import grpc

from ..persistence.watchers.kafka import KafkaOperationWatcher
from ..persistence.watchers.rabbitmq import RabbitMqOperationWatcher
from ..server.bots.instances.kafka import KafkaBotsInstance
from ..server.bots.instances.rabbitmq import RabbitMqBotsInstance
from ..server.cas.storage.remote import RemoteStorage
from ..server.execution.instances.kafka import KafkaExecutionInstance
from ..server.execution.instances.rabbitmq import RabbitMqExecutionInstance
from ..server.operations.instances.kafka import KafkaOperationsInstance
from ..server.operations.instances.rabbitmq import RabbitMqOperationsInstance
from ..server.server import Server
from ..settings import LOG_RECORD_FORMAT


class PrefixFilter(logging.Filter):
    def __init__(self, prefix, name='prefixes'):
        super().__init__(name=name)
        self._prefix = prefix

    def filter(self, record):
        return record.name.startswith(self._prefix)


@click.group()
def cli():
    """BuildGrid but with queues"""


def setup_logger():
    root_logger = logging.getLogger()
    log_handler = logging.StreamHandler(stream=sys.stdout)
    for log_filter in root_logger.filters:
        log_handler.addFilter(log_filter)
    log_handler.addFilter(PrefixFilter('buildgrid'))
    logging.basicConfig(format=LOG_RECORD_FORMAT, handlers=[log_handler])
    root_logger.setLevel(logging.DEBUG)


@cli.command()
@click.option('--cas', required=True, type=str,
              help="URL for a remote CAS server")
@click.option('--cas-instance-name', 'cas_instance_name', required=True, type=str,
              help="Instance name for the remote CAS")
@click.option('--mq-url', 'mq_url', required=True, type=str,
              help="URL for a Kafka bootstrap server/broker")
@click.option('--instance-name', 'instance_name', type=str,
              help="Instance name for this service")
@click.option('-p', '--port', 'port', type=int, default=50051)
@click.option('-m', '--mq', 'message_queue', type=str, required=True)
def execution(cas, cas_instance_name, mq_url, instance_name, port, message_queue):
    setup_logger()

    click.echo(f"Setting up an ExecutionService named [{instance_name}]")

    server = Server(port=port)

    url = urlparse(cas)
    remote = f"{url.hostname}:{url.port}"
    cas_channel = grpc.insecure_channel(remote)
    storage = RemoteStorage(cas_channel, cas_instance_name)

    if message_queue == 'kafka':
        instance = KafkaExecutionInstance(instance_name, storage, [], [mq_url])
    elif message_queue == 'rabbitmq':
        instance = RabbitMqExecutionInstance(instance_name, storage, [], mq_url)
    else:
        raise Exception(f"Unsupported message queue type: [{message_queue}]")
    server.add_execution_instance(instance, instance_name)

    try:
        server.start()

    except KeyboardInterrupt:
        pass

    finally:
        click.echo("Stopping...")
        server.stop()


@cli.command()
@click.option('--mq-url', 'mq_url', required=True, type=str,
              help="URL for a Kafka bootstrap server/broker")
@click.option('--instance-name', 'instance_name', type=str,
              help="Instance name for this service")
@click.option('-p', '--port', 'port', type=int, default=50054)
@click.option('--redis', 'redis_url', type=str,
              help="URL to a redis server to store operations in")
@click.option('-m', '--mq', 'message_queue', type=str, required=True)
def bots(mq_url, instance_name, port, redis_url, message_queue):
    setup_logger()

    click.echo(f"Setting up an BotsService named [{instance_name}]")

    server = Server(port=port)

    if message_queue == 'kafka':
        instance = KafkaBotsInstance(instance_name, [mq_url], redis_url)
    elif message_queue == 'rabbitmq':
        instance = RabbitMqBotsInstance(instance_name, mq_url, redis_url)
    else:
        raise Exception(f"Unsupported message queue type: [{message_queue}]")
    server.add_bots_instance(instance, instance_name)

    try:
        server.start()

    except KeyboardInterrupt:
        pass

    finally:
        click.echo("Stopping...")
        server.stop()


@cli.command()
@click.option('--mq-url', 'mq_url', required=True, type=str,
              help="URL for a Kafka bootstrap server/broker")
@click.option('--instance-name', 'instance_name', type=str,
              help="Instance name for this service")
@click.option('-p', '--port', 'port', type=int, default=50055)
@click.option('--redis', 'redis_url', type=str,
              help="URL to a redis server to store operations in")
@click.option('-m', '--mq', 'message_queue', type=str, required=True)
def operations(mq_url, instance_name, port, redis_url, message_queue):
    setup_logger()

    click.echo(f"Setting up an OperationsService named [{instance_name}]")

    server = Server(port=port)

    if message_queue == 'kafka':
        instance = KafkaOperationsInstance(instance_name, redis_url, [mq_url])
    elif message_queue == 'rabbitmq':
        instance = RabbitMqOperationsInstance(instance_name, redis_url, mq_url)
    else:
        raise Exception(f"Unsupported message queue type: [{message_queue}]")
    server.add_operations_instance(instance, instance_name)

    try:
        server.start()

    except KeyboardInterrupt:
        pass

    finally:
        click.echo("Stopping...")
        server.stop()


@cli.command()
@click.option('--mq-url', 'mq_url', required=True, type=str,
              help="URL for a Kafka bootstrap server/broker")
@click.option('--redis', 'redis_url', type=str,
              help="URL to a redis server to store operations in")
@click.option('-m', '--mq', 'message_queue', type=str, required=True)
def watcher(mq_url, redis_url, message_queue):
    setup_logger()

    click.echo(f"Running an operations watcher...")

    if message_queue == 'kafka':
        watcher = KafkaOperationWatcher(redis_url, [kafka_url])
    elif message_queue == 'rabbitmq':
        watcher = RabbitMqOperationWatcher(redis_url, mq_url)
    else:
        raise Exception(f"Unsupported message queue type: [{message_queue}]")

    try:
        watcher.persist_stream()

    except KeyboardInterrupt:
        pass

    finally:
        click.echo("Stopping...")
