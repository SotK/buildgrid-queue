import json
import hashlib
import sys
import time

import nsq
import requests

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action
from buildgrid._protos.google.longrunning.operations_pb2 import Operation


publish_url = f"http://{sys.argv[1]}:4151/pub"

def _handle_message(message):
    action_digest = hashlib.sha256(message.body).hexdigest()
    print(f"Received {message.body}")
    content = Action()
    content.ParseFromString(message.body)
    operation = Operation(name=action_digest, done=False)
    update = operation.SerializeToString()
    requests.post(publish_url, params={"topic": "operation-updates"}, data=update)
    print("Handling...")
    time.sleep(30)
    print("Done")
    operation = Operation(name=action_digest, done=True)
    update = operation.SerializeToString()
    requests.post(publish_url, params={"topic": "operation-updates"}, data=update)
    return True


def main():
    try:
        requirement = sys.argv[2]
    except IndexError:
        requirement = json.dumps({})
    topic = hashlib.sha1(requirement.encode("utf8")).hexdigest()
    reader = nsq.Reader(message_handler=_handle_message,
                        nsqd_tcp_addresses=[f"{sys.argv[1]}:4150"],
                        topic=topic,
                        channel=topic)

    print(f"Listening on {topic}...")
    nsq.run()

if __name__ == "__main__":
    main()
