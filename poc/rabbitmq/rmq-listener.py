import hashlib
import sys
import uuid

import pika


def main():
    broker = sys.argv[1]
    topic = "operation-updates"

    connection = pika.BlockingConnection(
        pika.URLParameters(broker))
    channel = connection.channel()
    channel.exchange_declare(exchange=topic, exchange_type='fanout')

    result = channel.queue_declare(queue='', exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange=topic, queue=queue_name)

    print(f"Listening on {topic}...")
    try:
        for method, _, message in channel.consume(queue_name):
            print(f"Received {message}")
            channel.basic_ack(method.delivery_tag)
    except KeyboardInterrupt:
        channel.close()
    connection.close()


if __name__ == "__main__":
    main()