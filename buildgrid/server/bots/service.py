# Copyright (C) 2018, 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
BotsService
=================

"""

import logging

import grpc

from google.protobuf import empty_pb2, timestamp_pb2

from ..._enums import BotStatus
from ..._exceptions import (
    InvalidArgumentError, OutOfSyncError, BotSessionClosedError,
    UnknownBotSessionError, BotSessionMismatchError
)
from ..._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import (
    BotSession, CreateBotSessionRequest, UpdateBotSessionRequest
)
from ..._protos.google.devtools.remoteworkers.v1test2 import bots_pb2_grpc
from .instances.base import BotsInstance


class BotsService(bots_pb2_grpc.BotsServicer):

    def __init__(self, server):
        self._logger = logging.getLogger(__name__)

        self._instances = {}

        bots_pb2_grpc.add_BotsServicer_to_server(self, server)

    def _get_instance(self, name: str) -> BotsInstance:
        try:
            return self._instances[name]

        except KeyError:
            raise InvalidArgumentError(f"Instance doesn't exist on server: [{name}]")

    def add_instance(self, instance_name: str, instance: BotsInstance) -> None:
        """Registers a new servicer instance.

        Args:
            instance_name (str): The new instance's name.
            instance (BotsInstance): The new instance itself.
        """
        self._instances[instance_name] = instance

    def CreateBotSession(self, request: CreateBotSessionRequest,
                         context: grpc.ServicerContext) -> BotSession:
        """Handles CreateBotSessionRequest messages.

        Args:
            request (CreateBotSessionRequest): The incoming RPC request.
            context (grpc.ServicerContext): Context for the RPC call.
        """
        self._logger.debug("CreateBotSession request from [%s]", context.peer())
        instance_name = request.parent

        try:
            instance = self._get_instance(instance_name)
            return instance.create_bot_session(request.bot_session)

        except InvalidArgumentError as e:
            self._logger.error(e)
            context.set_details(str(e))
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)

        return BotSession()

    def UpdateBotSession(self, request: UpdateBotSessionRequest,
                         context: grpc.ServicerContext) -> BotSession:
        """Handles UpdateBotSessionRequest messages.

        Args:
            request (UpdateBotSessionRequest): The incoming RPC request.
            context (grpc.ServicerContext): Context for the RPC call.
        """
        self._logger.debug("UpdateBotSession request from [%s]", context.peer())

        names = request.name.split("/")

        try:
            instance_name = '/'.join(names[:-1])

            deadline = context.time_remaining()
            if not deadline:
                deadline = 10
            instance = self._get_instance(instance_name)
            bot_session = instance.update_bot_session(
                request.bot_session,
                deadline=deadline)

            return bot_session

        except (InvalidArgumentError, BotSessionMismatchError, UnknownBotSessionError) as e:
            self._logger.error(e)
            context.set_details(str(e))
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)

        except (OutOfSyncError, BotSessionClosedError) as e:
            self._logger.error(e)
            context.set_details(str(e))
            context.set_code(grpc.StatusCode.DATA_LOSS)

        except NotImplementedError as e:
            self._logger.error(e)
            context.set_details(str(e))
            context.set_code(grpc.StatusCode.UNIMPLEMENTED)

        return BotSession()
