# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
from typing import List

from ..._enums import Topic
from ..._protos.google.longrunning.operations_pb2 import Operation
# TODO: This import path is a code smell
from ...server.utils.kafka import KafkaMixin
from .base import OperationWatcher


class KafkaOperationWatcher(OperationWatcher, KafkaMixin):

    def __init__(self, redis_url: str, bootstrap_servers: List[str]):
        OperationWatcher.__init__(self, redis_url)
        KafkaMixin.__init__(self, bootstrap_servers)

        self._logger = logging.getLogger(__name__)
        self._logger.info(f"Instantiated a watcher using {redis_url} for storage")

    def persist_stream(self):
        consumer = self._make_kafka_consumer(Topic.OPERATION_UPDATES.value,
                                             group_id=self.group_id)
        self._logger.debug(f"Created a consumer for {Topic.OPERATION_UPDATES.value}")
        for message in consumer:
            self._logger.debug(f"Received message [{message.value}]")
            operation = Operation()
            operation.ParseFromString(message.value)
            self._store_operation(operation.name, message.value)
