# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABC, abstractmethod
import bisect
import hashlib
from itertools import chain, combinations
import json
import logging
import threading
from typing import Dict, Iterator, List, Mapping, Optional, Tuple
import uuid

from redis import Redis

from ...._enums import LeaseState, OperationStage
from ...._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    Action, ActionResult, Digest, ExecuteResponse, ExecuteOperationMetadata
)
from ...._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import (
    BotSession, Lease
)
from ...._protos.google.longrunning.operations_pb2 import Operation
from ....client.actioncache import query
from ....client.channel import setup_channel


_state_to_stage = {
    LeaseState.UNSPECIFIED: OperationStage.UNKNOWN,
    LeaseState.PENDING: OperationStage.EXECUTING,
    LeaseState.ACTIVE: OperationStage.EXECUTING,
    LeaseState.COMPLETED: OperationStage.COMPLETED,
    LeaseState.CANCELLED: OperationStage.COMPLETED
}

AC_URL = "http://storage:50052/"


class BotNotifier:

    def __init__(self, name):
        self._action = None
        self._event = threading.Event()
        self._logger = logging.getLogger(__name__)
        self._name = name

    def send_action(self, action: Action) -> bool:
        if self._action is not None:
            self._logger.debug(f"Bot [{self._name}] is already being assigned an Action.")
            return False
        self._action = action
        self._logger.debug(f"Notifying bot [{self._name}] of Action assignment.")
        self._event.set()
        return True

    def wait_for_action(self, timeout=None) -> Action:
        if self._action is None:
            self._logger.info(
                f"Bot [{self._name}] waiting for up to [{timeout}] seconds "
                "for a runnable Action to become available.")
            self._event.wait(timeout=timeout)
        return self._action


class BotsInstance(ABC):

    def __init__(self, instance_name: str, redis_url: str):
        self._instance_name = instance_name
        self._redis_url = redis_url

        self._bots_by_capability = {}
        self._action_digest_cache = {}
        self._operation_name_by_lease_id = {}
        self._cancelled_operations = set()

    @abstractmethod
    def _send_operation_update(self, lease: Lease, force_send: bool=False) -> None:
        raise NotImplementedError()

    @abstractmethod
    def _listen_for_actions(self, capabilities_hash: str) -> None:
        raise NotImplementedError()

    @abstractmethod
    def _listen_for_cancellations(self) -> None:
        raise NotImplementedError()

    def _start_cancellation_listener(self) -> None:
        thread = threading.Thread(
            target=self._listen_for_cancellations,
            name="cancellation-listener",
            daemon=True)
        thread.start()

    def _get_worker_capabilities(self, bot_session: BotSession) -> Dict[str, List[str]]:
        capabilities = {}
        if bot_session.worker.devices:
            # According to the spec:
            #   "The first device in the worker is the "primary device" -
            #   that is, the device running a bot and which is
            #   responsible for actually executing commands."
            primary_device = bot_session.worker.devices[0]

            for device_property in primary_device.properties:
                if device_property.key not in capabilities:
                    capabilities[device_property.key] = []
                if device_property.value not in capabilities[device_property.key]:
                    bisect.insort(capabilities[device_property.key],
                                  device_property.value)
        self._logger.debug(f"Worker connected with capabilities: [{capabilities}]")
        return capabilities

    def _calculate_capabilities_hashes(
            self, capabilities: Mapping[str, List[str]]) -> Iterator[str]:
        # TODO: Cache this expensive mess
        pairs = [
            (key, value) for key, values in capabilities
            for value in values
        ]
        powerset = chain.from_iterable(combinations(pairs, r)
                                       for r in range(len(pairs) + 1))
        for group in powerset:
            capability_dict = {}
            for key, value in group:
                capability_dict.setdefault(key, []).append(value)
            capability_string = json.dumps(capability_dict, sort_keys=True)
            yield hashlib.sha1(capability_string.encode("utf8")).hexdigest()

    def _get_operation_name(self, lease: Lease) -> str:
        # First, try to get the name from the cached lease -> operation mapping
        if lease.id in self._operation_name_by_lease_id:
            return self._operation_name_by_lease_id[lease.id]

        # Next, try to get the name from the cached action -> digest mapping
        action = Action()
        lease.payload.Unpack(action)
        serialized_action = action.SerializeToString()
        if serialized_action in self._action_digest_cache:
            name = self._action_digest_cache[serialized_action]
            self._operation_name_by_lease_id[lease.id] = name
            return name

        # Finally, calculate the action digest and use that, caching the result
        action_digest = Digest()
        action_digest.hash = hashlib.sha256(serialized_action).hexdigest()
        action_digest.size_bytes = len(serialized_action)
        self._action_digest_cache[serialized_action] = action_digest.hash
        self._operation_name_by_lease_id[lease.id] = action_digest.hash
        return action_digest.hash

    def _construct_execute_response(self, lease: Lease,
                                    current_response: ExecuteResponse) -> ExecuteResponse:
        response = ExecuteResponse()
        if current_response:
            current_response.Unpack(response)

        response.status.CopyFrom(lease.status)
        if lease.status == LeaseState.CANCELLED.value:
            response.status.message = "Operation cancelled by client."

        result = ActionResult()
        if lease.result is not None and lease.result.Is(result.DESCRIPTOR):
            lease.result.Unpack(result)
            ac_channel, _ = setup_channel(AC_URL)
            with query(ac_channel, '') as action_cache:
                action = Action()
                lease.payload.Unpack(action)
                serialized_action = action.SerializeToString()
                action_digest = Digest()
                action_digest.hash = hashlib.sha256(serialized_action).hexdigest()
                action_digest.size_bytes = len(serialized_action)
                action_cache.update(action_digest, result)
        response.result.CopyFrom(result)
        response.cached_result = False

        # TODO: timestamps

        return response

    def _construct_operation_metadata(
            self, lease: Lease, action: Action,
            current_metadata: ExecuteOperationMetadata) -> ExecuteOperationMetadata:
        metadata = ExecuteOperationMetadata()
        if current_metadata:
            current_metadata.Unpack(metadata)
    
        metadata.stage = _state_to_stage[LeaseState(lease.state)].value

        serialized_action = action.SerializeToString()
        action_digest = Digest()
        # TODO: cache this calculation per-lease
        action_digest.hash = hashlib.sha256(serialized_action).hexdigest()
        action_digest.size_bytes = len(serialized_action)
        metadata.action_digest.CopyFrom(action_digest)
        return metadata

    def _get_or_create_operation(self, name: str) -> Operation:
        redis = Redis.from_url(self._redis_url)
        operation = Operation()
        serialized_operation = redis.get(f"operation:{name}")
        if serialized_operation is not None:
            operation.ParseFromString(serialized_operation)
        else:
            operation.name = name
        return operation

    def _construct_operation(self, lease: Lease) -> Tuple[Operation, bool]:
        # Unpack the Action to calculate the digest (and therefore the operation name)
        action = Action()
        lease.payload.Unpack(action)
        name = hashlib.sha256(action.SerializeToString()).hexdigest()
        operation = self._get_or_create_operation(name)
        original = operation.SerializeToString()

        lease_state = LeaseState(lease.state)
        operation.done = lease_state in (LeaseState.COMPLETED, LeaseState.CANCELLED)

        response = self._construct_execute_response(lease, operation.response)
        operation.response.Pack(response)

        metadata = self._construct_operation_metadata(lease, action, operation.metadata)
        operation.metadata.Pack(metadata)

        return operation, original != operation.SerializeToString()

    def _wait_for_lease(self, bot_session: BotSession, notifier: BotNotifier,
                        timeout: int=None) -> None:
        self._logger.info(
            f"Bot [{bot_session.name}] waiting for a lease for [{timeout}] seconds")
        action = notifier.wait_for_action(timeout=timeout)
        if action is not None:
            lease = Lease(id=str(uuid.uuid4()), state=LeaseState.PENDING.value)
            lease.payload.Pack(action)
            bot_session.leases.extend([lease])

            self._logger.debug(f"Created and assigned lease [{lease.id}]")
            self._send_operation_update(lease, force_send=True)
        else:
            self._logger.debug(f"Failed to get an action in [{timeout}] seconds")

    def _is_duplicate_action(self, action: Action) -> bool:
        redis = Redis.from_url(self._redis_url)
        name = hashlib.sha256(action.SerializeToString()).hexdigest()
        operation = redis.get(f"operation:{name}")
        # TODO: Check the action cache for completed operations
        return operation is not None

    def _create_capability_consumer(self, hash: str) -> None:
        self._logger.debug(f"Capability [{hash}] is new, starting a consumer")
        self._bots_by_capability[hash] = []

        thread = threading.Thread(
            target=self._listen_for_actions,
            name=f"consumer-{hash}",
            args=[hash])
        thread.start()

    def _request_leases(self, bot_session: BotSession, deadline: int=None) -> None:
        self._logger.info(f"Requesting a lease for bot [{bot_session.name}]")
        notifier = BotNotifier(bot_session.name)
        capabilities = self._get_worker_capabilities(bot_session)
        for hash in self._calculate_capabilities_hashes(capabilities):
            # TODO: This requires a lock
            if hash not in self._bots_by_capability:
                # Creating the consumer ensures that there is a list for this
                # capability in `self._bots_by_capability`, since it needs to
                # be there for the consumer thread to actually consume
                self._create_capability_consumer(hash)
            self._bots_by_capability[hash].append(notifier)

        # This method also handles assigning the lease to the bot_session, so we
        # don't need to do anything further
        self._wait_for_lease(bot_session, notifier, timeout=deadline)

    def _check_lease_state(self, lease: Lease) -> Optional[Lease]:
        lease_state = LeaseState(lease.state)
        if lease_state == LeaseState.CANCELLED:
            return None

        operation_name = self._get_operation_name(lease)
        if operation_name in self._cancelled_operations:
            self._cancelled_operations.remove(operation_name)
            lease.state = LeaseState.CANCELLED.value
            return lease

        if lease_state == LeaseState.COMPLETED:
            return None
        return lease

    def create_bot_session(self, bot_session: BotSession,
                           deadline: int=None) -> BotSession:
        """Create a new bot session for a bot.

        This method is called the first time a bot connects to the BotsService.
        It sets a name for the bot and attempts to assign a lease matching the
        bot's capabilities.

        Args:
            bot_session (BotSession): The BotSession object which contains the
                current state of the bot which has connected.
            deadline (int): The timeout used when waiting for a lease to be
                assigned to the bot.

        """
        bot_session.name = f"{self._instance_name}/{uuid.uuid4()}"

        self._request_leases(bot_session, deadline=deadline)

        return bot_session

    def update_bot_session(self, bot_session: BotSession,
                           deadline: int=None) -> BotSession:
        """Handle a bot providing an update on the state of its session.

        This method is called when a bot which has already connected once connects
        to the BotsService. It iterates over the leases assigned to the bot and sends
        updates on any relevant operations. It also cleans completed leases out of
        the session, and assigns a new lease if the bot has become idle.

        Args:
            bot_session (BotSession): The BotSession object which contains the
                current state of the bot which has connected.
            deadline (int): The timeout used when waiting for a lease to be
                assigned to the bot.

        """
        for lease in bot_session.leases:
            self._send_operation_update(lease)
            checked_lease = self._check_lease_state(lease)
            if not checked_lease:
                bot_session.leases.remove(lease)

        if not bot_session.leases:
            # TODO: Configurable timeout buffer here.
            if deadline and deadline > 5:
                deadline = deadline - 5
            self._request_leases(bot_session, deadline=deadline)
            # TODO: Handle disconnection with no new lease properly.

        return bot_session
