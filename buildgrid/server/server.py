# Copyright (C) 2018, 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
from concurrent import futures
import logging
import signal

import grpc

from .bots.service import BotsService
from .execution.service import ExecutionService
from .operations.service import OperationsService
from .capabilities.instance import CapabilitiesInstance
from .capabilities.service import CapabilitiesService


class Server:

    def __init__(self, port=50051):
        self._logger = logging.getLogger(__name__)
        self._main_loop = asyncio.get_event_loop()
        self._port = port

        # TODO: auth
        # TODO: configurable thread pool size + checking
        self._grpc_executor = futures.ThreadPoolExecutor(
            2000, thread_name_prefix="gRPC_Executor")
        self._grpc_server = grpc.server(self._grpc_executor,
                                        options=(('grpc.so_reuseport', 0),),
                                        maximum_concurrent_rpcs=2000)

        self._bots_service = BotsService(self._grpc_server)
        self._capabilities_service = CapabilitiesService(self._grpc_server)
        self._execution_service = ExecutionService(self._grpc_server)
        self._operations_service = OperationsService(self._grpc_server)

    def start(self):
        self._logger.info("Starting gRPC server")
        self._grpc_server.add_insecure_port(f'[::]:{self._port}')
        self._grpc_server.start()
        self._main_loop.add_signal_handler(signal.SIGTERM, self.stop)
        self._main_loop.run_forever()

    def stop(self):
        self._main_loop.stop()
        self._grpc_server.stop(None)

    def _add_capabilities_instance(self, instance_name,
                                   cas_instance=None,
                                   action_cache_instance=None,
                                   execution_instance=None):
        """Adds a :obj:`CapabilitiesInstance` to the service.

        Args:
            instance (:obj:`CapabilitiesInstance`): Instance to add.
            instance_name (str): Instance name.
        """

        try:
            if cas_instance:
                self._capabilities_service.add_cas_instance(
                    instance_name, cas_instance)
            if action_cache_instance:
                self._capabilities_service.add_action_cache_instance(
                    instance_name, action_cache_instance)
            if execution_instance:
                self._capabilities_service.add_execution_instance(
                    instance_name, execution_instance)

        except KeyError:
            capabilities_instance = CapabilitiesInstance(cas_instance,
                                                         action_cache_instance,
                                                         execution_instance)
            self._capabilities_service.add_instance(instance_name, capabilities_instance)

    def add_execution_instance(self, instance, instance_name):
        self._execution_service.add_instance(instance_name, instance)
        self._add_capabilities_instance(instance_name, execution_instance=instance)

    def add_bots_instance(self, instance, instance_name):
        self._bots_service.add_instance(instance_name, instance)

    def add_operations_instance(self, instance, instance_name):
        self._operations_service.add_instance(instance_name, instance)