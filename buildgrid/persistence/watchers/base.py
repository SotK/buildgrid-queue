# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABC, abstractmethod
from typing import List

from redis import Redis

from ..._enums import Topic


class OperationWatcher(ABC):

    group_id = Topic.OPERATION_UPDATES.value

    def __init__(self, redis_url: str):
        self._redis_url = redis_url

    def _store_operation(self, operation_name: str, operation: str) -> None:
        self._logger.info(f"Storing update for {operation_name}")
        redis = Redis.from_url(self._redis_url)
        redis.set(f"operation:{operation_name}", operation)
        redis.sadd("operations", f"operation:{operation_name}")

    @abstractmethod
    def persist_stream(self):
        """Persist state changes described in an Operation stream.

        This listens to the `operation-updates` topic on the configured
        message broker, and replicates any state changes described in the
        messages on that topic in the configured database.

        NOTE: Currently this is just Kafka -> Redis

        """
        raise NotImplementedError()
