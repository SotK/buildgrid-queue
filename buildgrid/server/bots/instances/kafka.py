# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import bisect
import hashlib
from itertools import chain, combinations
import json
import logging
import threading
import traceback
from typing import Dict, Iterator, List, Mapping, Optional
import uuid

from redis import Redis

from ...._enums import LeaseState, OperationStage, Topic
from ...._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action
from ...._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import BotSession, Lease
from ...._protos.google.longrunning.operations_pb2 import Operation
from ...utils.kafka import KafkaMixin
from .base import BotsInstance


class KafkaBotsInstance(BotsInstance, KafkaMixin):

    def __init__(self, instance_name: str, bootstrap_servers: List[str], redis_url: str):
        BotsInstance.__init__(self, instance_name, redis_url)
        KafkaMixin.__init__(self, bootstrap_servers)
        self._logger = logging.getLogger(__name__)

        self._start_cancellation_listener()

    def _listen_for_cancellations(self) -> None:
        group_id = str(uuid.uuid4())
        consumer = self._make_kafka_consumer(Topic.OPERATION_CANCEL.value,
                                             group_id=group_id)
        for message in consumer:
            self._cancelled_operations.add(message.value.decode("utf8"))

    def _send_operation_update(self, lease: Lease, force_send: bool=False) -> None:
        try:
            operation, modified = self._construct_operation(lease)
            if modified or force_send:
                # Set a key when sending the Operation message, to ensure
                # that messages are ordered correctly when received by consumers
                self._kafka_producer.send(Topic.OPERATION_UPDATES.value,
                                          operation.SerializeToString(),
                                          key=operation.name.encode('utf8'))
        except Exception as e:
            self._logger.error(f"Error when sending operation update: {e}")

    def _listen_for_actions(self, topic: str) -> None:
        self._logger.info(f"Creating a Kafka consumer for [{topic}]")
        consumer = self._make_kafka_consumer(topic, group_id=topic)

        while topic in self._bots_by_capability:
            self._logger.debug(f"Getting next message on {topic}")
            action_message = next(consumer)
            self._logger.debug(f"Received message on {topic}")
            action = Action()
            action.ParseFromString(action_message.value)

            if self._is_duplicate_action(action):
                self._logger.info(f"Deduplicated action on {topic}")
                continue

            # TODO: find a way to do this asynchronously, without 
            # acknowledging too early
            assigned = False
            while not assigned:
                notifier = self._bots_by_capability[topic].pop()
                assigned = notifier.send_action(action)

            # When there are no bots left, we need to either start caching
            # actions internally (probably not ideal, since if we die with
            # work cached but not assigned it will be lost) or block here
            # until bots are available again.
            # TODO: implement a communication method for the above
            if not self._bots_by_capability[topic]:
                self._logger.debug(f"No bots left for topic [{topic}]")
