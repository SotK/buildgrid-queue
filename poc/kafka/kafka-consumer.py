import json
import hashlib
import sys
import time

from kafka import KafkaConsumer, KafkaProducer

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action
from buildgrid._protos.google.longrunning.operations_pb2 import Operation


def _handle_message(message):
    print(f"Received {message.value}")
    content = Action()
    content.ParseFromString(message.value)
    yield content, 3
    print(f"{message.topic}: {content}")
    print("Handling...")
    time.sleep(30)
    print("Done")
    yield content, 4


def main():
    broker = sys.argv[1]
    try:
        requirement = sys.argv[2]
    except IndexError:
        requirement = json.dumps({})
    topic = hashlib.sha1(requirement.encode("utf8")).hexdigest()
    consumer = KafkaConsumer(topic, group_id=topic, bootstrap_servers=[broker])
    producer = KafkaProducer(bootstrap_servers=[broker])

    print(f"Listening on {topic}...")
    for message in consumer:
        for action, state in _handle_message(message):
            action_digest = hashlib.sha256(action.SerializeToString()).hexdigest()
            operation = Operation(name=action_digest, done=state==4)
            update = operation.SerializeToString()
            producer.send("operation-updates", update)

if __name__ == "__main__":
    main()
