# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABC, abstractmethod
import bisect
import hashlib
import json
from typing import Iterator, List

import grpc

from ...._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    Action, Command, Digest, DigestFunction
)
from ...._protos.google.longrunning.operations_pb2 import Operation
from ...cas.storage.storage_abc import StorageABC


class ExecutionInstance(ABC):

    def __init__(self, instance_name: str, storage: StorageABC,
                 supported_property_keys: List[str]):
        self.instance_name = instance_name

        self._property_keys = {"ISA", "OSFamily"}
        self._property_keys.update(supported_property_keys)
        self._unique_keys = {"OSFamily"}
        # TODO: Support user-defined unique keys

        self._storage = storage

    def _validate_command_requirements(self, command: Command) -> bool:
        # TODO: Check that specified properties are supported and
        # uniqueness constraints are met
        return True

    def _hash_command_requirements(self, command: Command) -> str:
        # Construct a dictionary of requirements from the Command
        requirements = {}
        for platform_property in command.platform.properties:
            if platform_property.name not in requirements:
                # We want this to behave as a set, but use a list because we will be
                # passing this to a JSON dumper imminently
                requirements[platform_property.name] = []
            if platform_property.value not in requirements[platform_property.name]:
                bisect.insort(requirements[platform_property.name],
                              platform_property.value)

        # TODO: Support configurable hashing algorithm
        reqs_string = json.dumps(requirements, sort_keys=True)
        return hashlib.sha1(reqs_string.encode("utf8")).hexdigest()

    def hash_type(self) -> DigestFunction:
        # TODO: Support more/configurable digest functions
        return DigestFunction.SHA256

    @abstractmethod
    def execute(self, action_digest: Digest, skip_cache_lookup: bool) -> str:
        """Send an action out for execution, and return the operation name

        The operation name is the name that will be used in updates sent about
        this action from BotsServices handling the execution.

        Args:
            action_digest (Digest): The digest of the action to be executed.
            skip_cache_lookup (bool): Whether to check the action cache before
                requesting execution of the action.

        Returns:
            str: The name of the operation representing the execution of the
                given action.

        """
        raise NotImplementedError()

    @abstractmethod
    def stream_operation_updates(self, operation_name: str,
                                 context: grpc.ServicerContext) -> Iterator[Operation]:
        """Stream updates for a given operation.

        Takes an operation name, and returns an iterator (probably a generator)
        of Operation messages containing updates on the state of the specified
        operation.

        Args:
            operation_name (str): The name of the operation to stream updates
                for.
            context (grpc.ServicerContext): The gRPC context for the client that
                is requesting a stream of updates.
        """
        raise NotImplementedError()

    @abstractmethod
    def handle_disconnect(self, operation_name: str, client: str) -> None:
        """Callback to handle any cleanup needed when clients disconnect.

        Args:
            operation_name (str): The name of the operation that the client
                was responsible for.
            client (str): The name of the client which has disconnected.

        """
        raise NotImplementedError()
