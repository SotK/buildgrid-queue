# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging

from ...._enums import Topic
from ...utils.rabbitmq import RabbitMqMixin
from .base import OperationsInstance


class RabbitMqOperationsInstance(OperationsInstance, RabbitMqMixin):

    def __init__(self, instance_name, redis_url, amqp_url):
        OperationsInstance.__init__(self, instance_name, redis_url)
        RabbitMqMixin.__init__(self, amqp_url)
        self._logger = logging.getLogger(__name__)

    def cancel_operation(self, operation_name):
        self._logger.info(f"Sending cancellation message for [{operation_name}]")
        self._publish_to_exchange(Topic.OPERATION_CANCEL.value,
                                  operation_name.encode('utf8'))