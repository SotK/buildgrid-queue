# Copyright (C) 2018, 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
ExecutionService
================

Serves remote execution requests.
"""

from functools import partial
import logging
from typing import Iterator

import grpc

from ..._protos.build.bazel.remote.execution.v2 import remote_execution_pb2_grpc
from ..._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import ExecuteRequest, WaitExecutionRequest
from ..._protos.google.longrunning.operations_pb2 import Operation
from .instances.base import ExecutionInstance


class ExecutionService(remote_execution_pb2_grpc.ExecutionServicer):

    def __init__(self, server):
        self._logger = logging.getLogger(__name__)

        self._peers_by_instance = None
        self._peers = None

        self._instances = {}

        remote_execution_pb2_grpc.add_ExecutionServicer_to_server(self, server)

    def _rpc_termination_callback(self, peer_uid: str, instance_name: str,
                                  operation_name: str) -> None:
        self._logger.debug(
            f"RPC terminated for peer_uid=[{peer_uid}], instance_name=[{instance_name}], "
            f"operation_name=[{operation_name}]")

        instance = self._get_instance(instance_name)
        instance.handle_disconnect(operation_name, peer_uid)

    def _get_instance(self, name: str) -> ExecutionInstance:
        try:
            return self._instances[name]

        except KeyError:
            raise Exception("Instance doesn't exist on server: [{}]".format(name))

    def add_instance(self, instance_name: str, instance: ExecutionInstance):
        """Registers a new servicer instance.

        Args:
            instance_name (str): The new instance's name.
            instance (ExecutionInstance): The new instance itself.
        """
        self._instances[instance_name] = instance

    def _stream_operation_updates(self, instance_name: str, operation_name:str,
                                  context: grpc.ServicerContext) -> Iterator[Operation]:
        instance = self._get_instance(instance_name)

        # Add a callback to run on termination of the gRPC connection.
        # This callback takes care of killing the thread being used to
        # handle the request.
        context.add_callback(partial(self._rpc_termination_callback,
                                     context.peer(), instance_name, operation_name))

        # Stream updates for this operation until either it is done or
        # the connection is closed.
        self._logger.info(f"Streaming updates for operation [{operation_name}]")
        yield from instance.stream_operation_updates(operation_name, context)
        self._logger.info(f"Completed streaming updates for [{operation_name}]")

        if not context.is_active():
            self._logger.info("Peer peer_uid=[%s] was holding up a thread for "
                              "`stream_operation_updates()` for instance_name=[%s], "
                              "operation_name=[%s], but the rpc context is not active anymore; "
                              "releasing thread.",
                              context.peer(), instance_name, operation_name)

    def Execute(self, request: ExecuteRequest,
                context: grpc.ServicerContext) -> Iterator[Operation]:
        """Handles ExecuteRequest messages.

        Args:
            request (ExecuteRequest): The incoming RPC request.
            context (grpc.ServicerContext): Context for the RPC call.
        """
        self._logger.info(f"Received Execute request from [{context.peer()}]")
        instance_name = request.instance_name
        instance = self._get_instance(instance_name)

        # Arrange for the Action to be executed (or look up the result in
        # the ActionCache)
        operation_name = instance.execute(
            request.action_digest, request.skip_cache_lookup)

        yield from self._stream_operation_updates(instance_name, operation_name, context)

    def WaitExecution(self, request: WaitExecutionRequest,
                      context: grpc.ServicerContext) -> Iterator[Operation]:
        """Handles WaitExecutionRequest messages.

        Args:
            request (WaitExecutionRequest): The incoming RPC request.
            context (grpc.ServicerContext): Context for the RPC call.
        """
        self._logger.debug(f"Received WaitExecution request from [{context.peer()}]")
        names = request.name.split('/')
        instance_name = '/'.join(names[:-1])
        operation_name = request.name

        yield from self._stream_operation_updates(instance_name, operation_name, context)
