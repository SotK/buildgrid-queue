# BuildGrid Queue prototype

This is a prototype of a REAPI/RWAPI implementation loosely based on
https://gitlab.com/BuildGrid/buildgrid which utilises Kafka queues to
handle job scheduling and update streaming.


## Usage

The test prototypes provide an Execution service available on
`localhost:50051`, a CAS and AC service available on
`localhost:50052`, and an Operation Service on `localhost:50055`.

To run the Kafka prototype:

    docker-compose -f docker-compose.kafka.yml up

To run the RabbitMQ prototype:

    docker-compose -f docker-compose.rabbitmq.yml up


## DrawIO

To view the `.drawio` file, go to https://www.draw.io/, select "Device"
as the storage, and then "Open Existing Diagram", selecting the file.
