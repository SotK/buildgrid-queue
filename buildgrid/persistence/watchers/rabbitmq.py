# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging

from ..._enums import Topic
from ..._protos.google.longrunning.operations_pb2 import Operation
# TODO: This import path is a code smell
from ...server.utils.rabbitmq import RabbitMqMixin
from .base import OperationWatcher


class RabbitMqOperationWatcher(OperationWatcher, RabbitMqMixin):

    def __init__(self, redis_url: str, amqp_url: str):
        OperationWatcher.__init__(self, redis_url)
        RabbitMqMixin.__init__(self, amqp_url)

        self._logger = logging.getLogger(__name__)
        self._logger.info(f"Instantiated a watcher using {redis_url} for storage")

    def persist_stream(self):
        self._logger.debug(f"Creating a consumer for {Topic.OPERATION_UPDATES.value}")
        for message in self._consume_from_exchange(Topic.OPERATION_UPDATES.value):
            self._logger.debug(f"Received message [{message}]")
            operation = Operation()
            operation.ParseFromString(message)
            self._store_operation(operation.name, message)
