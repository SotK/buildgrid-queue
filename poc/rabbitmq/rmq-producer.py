import hashlib
import sys

import pika

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action, Digest


def _create_action(command):
    command_hash = hashlib.sha1(command.encode("utf8")).hexdigest()
    command_digest = Digest(hash=command_hash, size_bytes=len(command))
    action = Action(command_digest=command_digest)
    return action


def main():
    broker = sys.argv[1]
    connection = pika.BlockingConnection(
        pika.URLParameters(broker))
    channel = connection.channel()
    message = input("> ")
    while message:
        command, requirement = message.strip().split("|")
        action = _create_action(command)
        topic = hashlib.sha1(requirement.encode("utf8")).hexdigest()
        channel.queue_declare(topic)

        channel.basic_publish(exchange='', routing_key=topic, body=action.SerializeToString())

        print(f"Sent {action.SerializeToString()} to {topic}")
        message = input("> ")


if __name__ == "__main__":
    main()
