# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import hashlib
import logging
from typing import Iterator, List

import grpc

from ...._enums import Topic
from ...._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    Action, Command, Digest, DigestFunction
)
from ...._protos.google.longrunning.operations_pb2 import Operation
from ....utils import retry
from ...utils.rabbitmq import RabbitMqMixin
from ...cas.storage.storage_abc import StorageABC
from .base import ExecutionInstance


class RabbitMqExecutionInstance(ExecutionInstance, RabbitMqMixin):

    """ExecutionService implementation using RabbitMQ as a queue backend."""

    def __init__(self, instance_name: str, storage: StorageABC,
                 supported_property_keys: List[str], amqp_url: str):
        ExecutionInstance.__init__(self, instance_name, storage, supported_property_keys)
        RabbitMqMixin.__init__(self, amqp_url)

        self._logger = logging.getLogger(__name__)

    def execute(self, action_digest: Digest, skip_cache_lookup: bool) -> str:
        action = self._storage.get_message(action_digest, Action)
        command = self._storage.get_message(action.command_digest, Command)

        if not self._validate_command_requirements(command):
            # TODO: Raise a more useful exception here
            raise Exception("invalid command platform requirements")

        queue_name = self._hash_command_requirements(command)
        self._publish_to_queue(queue_name, action.SerializeToString())
        self._logger.debug(
            f"Sent [{action_digest.hash}/{action_digest.size_bytes}] to [{queue_name}]")

        return f"{self.instance_name}{'/' if self.instance_name else ''}{action_digest.hash}"

    def stream_operation_updates(self, operation_name: str,
                                 context: grpc.ServicerContext) -> Iterator[Operation]:
        self._logger.debug(f"Streaming updates for operation [{operation_name}]")

        for message in self._consume_from_exchange(Topic.OPERATION_UPDATES.value):
            self._logger.debug(
                f"Received message [{message}] on [{Topic.OPERATION_UPDATES.value}]")

            # TODO: avoid needing to deserialise every message?
            operation = Operation()
            operation.ParseFromString(message)
            self._logger.debug(f"Message for operation [{operation.name}], "
                               f"listening for [{operation_name}]")

            # Stop consuming if the client has closed the connection
            if not context.is_active():
                break

            # Skip this operation if it isn't the one we care about
            if operation.name != operation_name:
                continue

            yield operation

            # If the operation is finished we also don't need to consume anymore
            if operation.done:
                break

    def handle_disconnect(self, operation_name: str, peer: str):
        # TODO: Decide if we care about interrupting stream_operation_updates here
        pass