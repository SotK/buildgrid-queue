# Copyright (C) 2018, 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
OperationsInstance
==================
An instance of the LongRunningOperations Service.
"""

from abc import ABC, abstractmethod
import logging

from redis import Redis

from ...._exceptions import InvalidArgumentError, NotFoundError
from ...._protos.google.longrunning.operations_pb2 import Operation, ListOperationsResponse


class OperationsInstance(ABC):

    def __init__(self, instance_name: str, redis_url: str):
        self._logger = logging.getLogger(__name__)
        self._instance_name = instance_name
        self._redis_url = redis_url

    def get_operation(self, operation_name: str) -> Operation:
        redis = Redis.from_url(self._redis_url)
        serialized_operation = redis.get(f"operation:{operation_name}")
        if serialized_operation is None:
            raise InvalidArgumentError(f"Operation name does not exist: [{operation_name}]")

        operation = Operation()
        operation.ParseFromString(serialized_operation)
        return operation

    # TODO: work out what types these should have
    def list_operations(self, list_filter, page_size, page_token) -> ListOperationsResponse:
        # TODO: Pages
        # Spec says number of pages and length of a page are optional
        response = ListOperationsResponse()

        redis = Redis.from_url(self._redis_url)
        operation_keys = redis.smembers("operations")
        self._logger.info(f"Retrieved operations: {operation_keys}")

        operations = []
        for key in operation_keys:
            serialized_operation = redis.get(key)
            operation = Operation()
            operation.ParseFromString(serialized_operation)
            operations.append(operation)

        response.operations.extend(operations)

        return response

    # TODO: Support this maybe?
    def delete_operation(self, operation_name):
        raise NotImplementedError()

    @abstractmethod
    def cancel_operation(self, operation_name):
        raise NotImplementedError()
