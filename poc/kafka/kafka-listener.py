import hashlib
import sys
import uuid

from kafka import KafkaConsumer


def main():
    broker = sys.argv[1]
    topic = "operation-updates"
    group = str(uuid.uuid4())
    consumer = KafkaConsumer(topic, group_id=group, bootstrap_servers=[broker])
    print(f"Listening on {topic}...")
    for message in consumer:
        print(f"Received {message.value}")


if __name__ == "__main__":
    main()