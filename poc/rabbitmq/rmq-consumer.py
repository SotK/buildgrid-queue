import json
import hashlib
import sys
import time

import pika

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action
from buildgrid._protos.google.longrunning.operations_pb2 import Operation


def _send_operation_update(channel, action, state):
    action_digest = hashlib.sha256(action.SerializeToString()).hexdigest()
    operation = Operation(name=action_digest, done=state==4)
    update = operation.SerializeToString()
    channel.basic_publish(
        exchange="operation-updates",
        routing_key="",
        body=update)

def _handle_message(channel, method, properties, message):
    print(f"Received {message}")
    content = Action()
    content.ParseFromString(message)
    channel.basic_ack(method.delivery_tag)
    _send_operation_update(channel, content, 3)
    print(f"{method.delivery_tag}: {content}")
    print("Handling...")
    time.sleep(30)
    print("Done")
    _send_operation_update(channel, content, 4)


def main():
    broker = sys.argv[1]
    try:
        requirement = sys.argv[2]
    except IndexError:
        requirement = json.dumps({})
    topic = hashlib.sha1(requirement.encode("utf8")).hexdigest()

    print(f"Listening on {topic}...")
    connection = pika.BlockingConnection(
        pika.URLParameters(broker))
    channel = connection.channel()
    channel.exchange_declare(exchange='operation-updates', exchange_type='fanout')
    channel.queue_declare(topic)
    channel.basic_consume(topic, _handle_message)
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    connection.close()


if __name__ == "__main__":
    main()
