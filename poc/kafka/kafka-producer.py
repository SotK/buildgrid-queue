import hashlib
import sys

from kafka import KafkaProducer
from kafka.errors import KafkaError

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action, Digest


def _create_action(command):
    command_hash = hashlib.sha1(command.encode("utf8")).hexdigest()
    command_digest = Digest(hash=command_hash, size_bytes=len(command))
    action = Action(command_digest=command_digest)
    return action


def main():
    broker = sys.argv[1]
    producer = KafkaProducer(bootstrap_servers=[broker])
    message = input("> ")
    while message:
        command, requirement = message.strip().split("|")
        action = _create_action(command)
        topic = hashlib.sha1(requirement.encode("utf8")).hexdigest()
        producer.send(topic, action.SerializeToString())
        print(f"Sent {action.SerializeToString()} to {topic}")
        message = input("> ")


if __name__ == "__main__":
    main()
