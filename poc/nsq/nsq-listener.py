import hashlib
import sys
import uuid

import nsq


def _handle_message(message):
    print(f"Received {message.body}")
    return True


def main():
    nsqd = sys.argv[1]
    topic = "operation-updates"
    channel = str(uuid.uuid4())
    reader = nsq.Reader(message_handler=_handle_message,
                        nsqd_tcp_addresses=[nsqd],
                        topic=topic,
                        channel=channel)
    print(f"Listening on {topic}...")
    nsq.run()


if __name__ == "__main__":
    main()