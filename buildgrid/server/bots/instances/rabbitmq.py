# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
import uuid

from pika.exceptions import AMQPConnectionError
from redis import Redis

from ...._enums import LeaseState, OperationStage, Topic
from ...._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action
from ...._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import BotSession, Lease
from ...._protos.google.longrunning.operations_pb2 import Operation
from ...utils.rabbitmq import RabbitMqMixin
from .base import BotsInstance


class RabbitMqBotsInstance(BotsInstance, RabbitMqMixin):

    def __init__(self, instance_name: str, amqp_url: str, redis_url: str):
        BotsInstance.__init__(self, instance_name, redis_url)
        RabbitMqMixin.__init__(self, amqp_url)
        self._logger = logging.getLogger(__name__)
        self._start_cancellation_listener()

    def _listen_for_cancellations(self) -> None:
        for message in self._consume_from_exchange(Topic.OPERATION_CANCEL.value):
            self._logger.info(f"Received cancellation notification for [{message}]")
            self._cancelled_operations.add(message.decode('utf8'))

    def _send_operation_update(self, lease: Lease, force_send: bool=False) -> None:
        try:
            operation, modified = self._construct_operation(lease)
            if modified:
                self._publish_to_exchange(Topic.OPERATION_UPDATES.value,
                                            operation.SerializeToString())
        except Exception as e:
            self._logger.error(f"Error when sending operation update: {e}")

    def _listen_for_actions(self, queue_name: str) -> None:
        self._logger.info(f"Creating a RabbitMQ consumer for [{queue_name}]")
        with self._get_rabbitmq_connection() as connection:
            channel = connection.channel()
            channel.queue_declare(queue_name)
            self._logger.debug(f"Consuming from queue [{queue_name}]")
            for method, _, action_message in channel.consume(queue_name):
                self._logger.debug(f"Received message on [{queue_name}]")
                action = Action()
                action.ParseFromString(action_message)

                assigned = False
                while not assigned:
                    notifier = self._bots_by_capability[queue_name].pop()
                    assigned = notifier.send_action(action)
                if assigned:
                    channel.basic_ack(method.delivery_tag)

                if not self._bots_by_capability[queue_name]:
                    self._logger.debug(f"No bots left for topic [{queue_name}]")
            self._logger.debug("Finished consuming. SHOULD NEVER GET HERE.")