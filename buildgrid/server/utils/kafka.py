# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Kafka-related utility classes and functions"""


from typing import List

from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import NoBrokersAvailable

from ...utils import retry


class KafkaMixin:

    def __init__(self, bootstrap_servers: List[str]):
        self._kafka_bootstrap_servers = bootstrap_servers
        self._kafka_producer = self._make_kafka_producer()

    @retry(exceptions=NoBrokersAvailable)
    def _make_kafka_producer(self) -> KafkaProducer:
        return KafkaProducer(bootstrap_servers=self._kafka_bootstrap_servers)

    @retry(exceptions=NoBrokersAvailable)
    def _make_kafka_consumer(self, *args, **kwargs) -> KafkaConsumer:
        return KafkaConsumer(*args, bootstrap_servers=self._kafka_bootstrap_servers,
                             **kwargs)