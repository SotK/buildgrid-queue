# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""RabbitMQ-related utility classes and functions"""


from contextlib import contextmanager
import logging
import traceback
from typing import Iterator, List

import pika

from ...utils import retry


class RabbitMqMixin:

    def __init__(self, amqp_url: str):
        self._amqp_url = amqp_url

    @contextmanager
    def _get_rabbitmq_connection(self) -> pika.BlockingConnection:
        connection = None
        while not connection:
            try:
                connection = pika.BlockingConnection(pika.URLParameters(self._amqp_url))
            except:
                continue
        try:
            yield connection
        except:
            self._logger.debug("RabbitMQ error...")
            self._logger.debug(traceback.format_exc())
        finally:
            connection.close()

    @retry()
    def _publish_to_queue(self, queue_name: str, message: str) -> None:
        # TODO: reuse this connection on a per-thread basis
        connection = pika.BlockingConnection(pika.URLParameters(self._amqp_url))
        channel = connection.channel()

        channel.queue_declare(queue_name)

        channel.basic_publish(exchange='',
                              routing_key=queue_name,
                              body=message)
        connection.close()

    @retry()
    def _publish_to_exchange(self, exchange_name: str, message: str) -> None:
        # TODO: reuse this connection on a per-thread basis
        connection = pika.BlockingConnection(pika.URLParameters(self._amqp_url))
        channel = connection.channel()

        # TODO: does this method need to support other exchange types?
        # probably not but to be considered
        channel.exchange_declare(exchange=exchange_name, exchange_type='fanout')

        channel.basic_publish(exchange=exchange_name, routing_key='', body=message)
        connection.close()

    @retry()
    def _consume_from_queue(self, queue_name: str) -> Iterator[str]:
        self._logger.info(f"DEBUG: consuming from queue {queue_name}")
        # TODO: reuse this connection on a per-thread basis
        connection = pika.BlockingConnection(pika.URLParameters(self._amqp_url))
        self._logger.info(f"DEBUG: made connection")
        channel = connection.channel()
        self._logger.info(f"DEBUG: made channel")

        channel.queue_declare(queue_name)
        self._logger.info(f"DEBUG: declared queue {queue_name}")

        # TODO: this is pretty bad and will collapse in a mess if the connection
        # is temporarily lost. acceptable prototype but not production ready at all.
        try:
            for method, _, message in channel.consume(queue_name):
                # TODO: need a way to ack after handling reliably
                channel.basic_ack(method.delivery_tag)
                yield message
        except:
            self._logger.info(f"DEBUG: caught exception, closing")
            import traceback
            self._logger.error(traceback.format_exc())
            channel.close()
        connection.close()

    @retry()
    def _consume_from_exchange(self, exchange_name: str) -> Iterator[str]:
        # TODO: reuse this connection on a per-thread basis
        with self._get_rabbitmq_connection() as connection:
            channel = connection.channel()

            # TODO: does this method need to support other exchange types?
            # probably not but to be considered
            channel.exchange_declare(exchange=exchange_name, exchange_type='fanout')

            result = channel.queue_declare(queue='', exclusive=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange=exchange_name, queue=queue_name)

            # TODO: this is pretty bad and will collapse in a mess if the connection
            # is temporarily lost. acceptable prototype but not production ready at all.
            for method, _, message in channel.consume(queue_name):
                channel.basic_ack(method.delivery_tag)
                yield message
