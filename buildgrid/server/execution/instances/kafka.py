# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import bisect
import hashlib
import json
import logging
from typing import Iterator, List
import uuid

import grpc

from ...._enums import Topic
from ...._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    Action, Command, Digest, DigestFunction
)
from ...._protos.google.longrunning.operations_pb2 import Operation
from ....utils import retry
from ...utils.kafka import KafkaMixin
from ...cas.storage.storage_abc import StorageABC
from .base import ExecutionInstance


class KafkaExecutionInstance(ExecutionInstance, KafkaMixin):

    """ExecutionService implementation using Kafka as a queue backend."""

    def __init__(self, instance_name: str, storage: StorageABC,
                 supported_property_keys: List[str], bootstrap_servers: List[str]):
        ExecutionInstance.__init__(self, instance_name, storage, supported_property_keys)
        KafkaMixin.__init__(self, bootstrap_servers)

        self._logger = logging.getLogger(__name__)

    def execute(self, action_digest: Digest, skip_cache_lookup: bool) -> str:
        action = self._storage.get_message(action_digest, Action)
        command = self._storage.get_message(action.command_digest, Command)

        # TODO: Look for a result in the ActionCache

        if not self._validate_command_requirements(command):
            # TODO: Raise a more useful exception here
            raise Exception("invalid command platform requirements")

        topic = self._hash_command_requirements(command)
        # TODO: Check if the topic exists, and warn if not.
        # Non-existent topic implies the action may never get executed.
        self._kafka_producer.send(topic, action.SerializeToString())
        self._logger.debug(
            f"Sent [{action_digest.hash}/{action_digest.size_bytes}] to [{topic}]")

        # TODO: Either support external storage of operation -> action mapping,
        # or pick a nicer method than this
        return f"{self.instance_name}{'/' if self.instance_name else ''}{action_digest.hash}"

    def stream_operation_updates(self, operation_name: str,
                                 context: grpc.ServicerContext) -> Iterator[Operation]:
        # Specify a unique group identifier so that we read from all partitions
        group = str(uuid.uuid4())
        consumer = self._make_kafka_consumer(Topic.OPERATION_UPDATES.value,
                                             group_id=group)
        self._logger.debug(f"Streaming updates for operation [{operation_name}]")

        # TODO: Support sending an initial message with the current operation state

        for message in consumer:
            self._logger.debug(f"Received message [{message.value}]")
            # TODO: Consider encoding the operation name in the message to avoid
            # deserializing every operation update
            operation = Operation()
            operation.ParseFromString(message.value)
            self._logger.debug(f"Message for operation [{operation.name}], "
                               f"listening for [{operation_name}]")
            if operation.name != operation_name:
                continue

            yield operation

            if operation.done or not context.is_active():
                break

    def handle_disconnect(self, operation_name: str, peer: str):
        # TODO: We might want to report some metrics here?
        # TODO: Decide if we care about poking the `stream_operation_updates` loop
        # here somehow to actively stop the thread
        pass
