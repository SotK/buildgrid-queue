# Pros and Cons of Various Queues

## General tradeoffs

- We can't entirely remove a persistent external storage system
    - We can take a microservice-ish approach and encapsulate
      it entirely in the OperationService, which is the only
      piece that needs it.


## Apache Kafka

### Pros

- Persistent topics
    - This gives us the option to replay history if needed
    - Also means we don't lose jobs if all the BotsServices
      simultaneously explode, unless Kafka also does before
      persisting a message
- Easy to use both pub/sub and queue-style messaging
- Lovely Python API
- Apparently scales up to about 200,000 partitions per cluster,
  with us caring about a single platform property this is far
  in excess of what I expect we'll need

### Cons

- Partition "limit" of 100 * broker count * replication factor
- Parallelism based on partitions, so need at least as many
  partitions in a topic as there are consumers in the largest
  consumer group of that topic
    - This means that the random choice of partition made when
      enqueuing Actions effectively decides which set of workers
      get the opportunity to execute that Action
    - If all those workers are busy, we could get stuck...
        - It will be doable to work around this, but it adds a bit
          of extra complexity
- No mechanism for priority-based delivery at the level of granularity
  described in the REAPI spec
    - We can mitigate this by providing a configurable number of
      queues which we read from at different rates to simulate
      having some block priority levels
    